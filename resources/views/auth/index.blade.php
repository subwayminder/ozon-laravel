@extends ('layout')

@section('content')
    <form action="" method="post">
        {{ csrf_field() }}
        <input type="text" name="portal" placeholder="Адрес портала">
        <input type="submit" value="Авторизоваться">
    </form>
    @foreach($data as $key=>$pos)
        {{$key}} => {{$pos}} <div>      </div>
    @endforeach
    {{$query_data}}
@endsection
