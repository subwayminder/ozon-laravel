@extends ('layout')

@section('content')
    <ul>
        <li><a href="/test=user.current">Информация о пользователе</a>
        <li><a href="/?test=user.update">Загрузить новую аватарку пользователя</a>
        <li><a href="/?test=log.blogpost.add">Опубликовать запись в Живой Ленте</a>
        <li><a href="/?test=event.bind">Установить обработчик события</a>
    </ul>

    <a href="/?refresh=1">Обновить данные авторизации</a><br />
    <a href="/?clear=1">Очистить данные авторизации</a><br />
@endsection
